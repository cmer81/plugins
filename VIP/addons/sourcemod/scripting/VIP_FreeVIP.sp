#pragma semicolon 1
#include <sourcemod>
#include <vip_core>

public Plugin:myinfo = 
{
	name = "[VIP] Free VIP",
	author = "inGame",
	version = "0.1"
};

new String:g_sGroup[64];

public OnPluginStart()
{
	new Handle:hCvar = CreateConVar("sm_vip_time_group", "NewYear_VIP", "Группа VIP-статуса");
	HookConVarChange(hCvar, OnGroupChange);
	GetConVarString(hCvar, g_sGroup, sizeof(g_sGroup));

	AutoExecConfig(true, "FreeVIP", "vip");
}

public OnGroupChange(Handle:hCvar, const String:sOldValue[], const String:sNewValue[])			strcopy(g_sGroup, sizeof(g_sGroup), sNewValue);

public OnRebuildAdminCache(AdminCachePart:part)
{
	Reload();
}

void Reload()
{
	for(new i = 1; i < MaxClients; ++i)
	{
		if(IsClientInGame(i) && !IsFakeClient(i) && VIP_IsClientVIP(i)) 
			VIP_OnVIPClientLoaded(i);
	}
}

public VIP_OnVIPClientLoaded(client)
{
	CreateTimer(0.1, Timer_Delay, GetClientUserId(client), TIMER_FLAG_NO_MAPCHANGE);
}

public OnClientPutInServer(client)
{
	if (!IsFakeClient(client)) 
	{
		CreateTimer(0.1, Timer_Delay, GetClientUserId(client), TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action:Timer_Delay(Handle:hTimer, any:UserID)
{
	new client = GetClientOfUserId(UserID);
	if (client && VIP_IsClientVIP(client) == false)
	{
		VIP_SetClientVIP(client, 0, _, g_sGroup, false);
	}
}


