#pragma semicolon 1

#include <sourcemod>
#include <multicolors>

#pragma newdecls required

#define MAX_BUFFER_SIZE		1024

#define REBUILD_CACHE_WAIT_TIME 2.0

GroupId UNDEFINED_GROUP_ID = view_as<GroupId>(-2);
AdminId UNDEFINED_ADMIN_ID = view_as<AdminId>(-2);

GroupId	g_gGroups[MAXPLAYERS+1];
AdminId	g_gAdmins[MAXPLAYERS+1][MAXPLAYERS+1];

char	g_sResolvedAdminGroups[MAXPLAYERS+1][MAX_BUFFER_SIZE];
int 	g_iResolvedAdminGroupsLength = 0;

ConVar g_cAdminsRealNames;

bool g_bReloadAdminList = false;
bool g_bMapEnd = false;

public Plugin myinfo =
{
	name = "Advanced Admin List",
	author = "maxime1907",
	description = "An advanced admin list system",
	version = "1.5",
	url = ""
};

public void OnPluginStart()
{
	g_cAdminsRealNames = CreateConVar("sm_admins_real_names", "0", "0 = disabled, 1 = enable in game admin name display", 0, true, 0.0, true, 1.0);

	g_cAdminsRealNames.AddChangeHook(OnCvarChanged);

	ReloadAdminList();
	
	AddCommandListener(Command_Admins, "sm_admins");

	AutoExecConfig(true);
}

public void OnCvarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	ReloadAdminList();
}

public void OnRebuildAdminCache(AdminCachePart part)
{
	// Only do something if admins are being rebuild
	if (part != AdminCache_Admins)
		return;

	CreateTimer(REBUILD_CACHE_WAIT_TIME, Timer_RebuildCache);
}

public Action Timer_RebuildCache(Handle hTimer)
{
	ReloadAdminList();
	return Plugin_Stop;
}

public void OnMapStart()
{
	g_bMapEnd = false;
}

public void OnMapEnd()
{
	g_bMapEnd = true;
}

public void OnClientPostAdminCheck(int client)
{
	if (g_bMapEnd)
		return;

	AdminId aid = GetUserAdmin(client);

	if (GetAdminFlag(aid, Admin_Generic))
		ReloadAdminList();
}

public void OnClientDisconnect(int client)
{
	if (g_bMapEnd)
		return;

	AdminId aid = GetUserAdmin(client);

	if (GetAdminFlag(aid, Admin_Generic))
		g_bReloadAdminList = true;
}

public void OnClientDisconnect_Post(int client)
{
	if (!g_bMapEnd && g_bReloadAdminList)
		ReloadAdminList();
}


//   .d8888b.   .d88888b.  888b     d888 888b     d888        d8888 888b    888 8888888b.   .d8888b.
//  d88P  Y88b d88P" "Y88b 8888b   d8888 8888b   d8888       d88888 8888b   888 888  "Y88b d88P  Y88b
//  888    888 888     888 88888b.d88888 88888b.d88888      d88P888 88888b  888 888    888 Y88b.
//  888        888     888 888Y88888P888 888Y88888P888     d88P 888 888Y88b 888 888    888  "Y888b.
//  888        888     888 888 Y888P 888 888 Y888P 888    d88P  888 888 Y88b888 888    888     "Y88b.
//  888    888 888     888 888  Y8P  888 888  Y8P  888   d88P   888 888  Y88888 888    888       "888
//  Y88b  d88P Y88b. .d88P 888   "   888 888   "   888  d8888888888 888   Y8888 888  .d88P Y88b  d88P
//   "Y8888P"   "Y88888P"  888       888 888       888 d88P     888 888    Y888 8888888P"   "Y8888P"
//

public Action Command_Admins(int client, const char[] command, int argc)
{
	if (!IsValidClient(client))
		return Plugin_Continue;

	printAdminList(client, g_sResolvedAdminGroups, g_iResolvedAdminGroupsLength);
	return Plugin_Stop;
}

// ######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
// ##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
// ##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
// ######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
// ##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
// ##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
// ##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######

public void printAdminList(int client, char[][] resolveAdminsAndGroups, int resolvedAdminGroupsLength)
{
	CPrintToChat(client, "{green}[Source Mod] {lightgreen}Admins %s", resolvedAdminGroupsLength <= 0 ? "are offline" : "currently online:");

	for (int i = 0; i < resolvedAdminGroupsLength; i++)
		CPrintToChat(client, resolveAdminsAndGroups[i]);
}

public void ReloadAdminList()
{
	reloadAdminList(g_gGroups, g_gAdmins, g_sResolvedAdminGroups, g_iResolvedAdminGroupsLength);
}

public void reloadAdminList(GroupId[] groups, AdminId[][] names, char resolvedAdminGroups[MAXPLAYERS+1][MAX_BUFFER_SIZE], int &resolvedAdminGroupsLength)
{
	initAdminsAndGroups(groups, names, resolvedAdminGroups, resolvedAdminGroupsLength);
	getAdminsAndGroups(groups, names);
	resolveAdminsAndGroups(groups, names, resolvedAdminGroups, resolvedAdminGroupsLength);
	g_bReloadAdminList = false;
}

public void initAdminsAndGroups(GroupId[] groups, AdminId[][] names, char resolvedAdminGroups[MAXPLAYERS+1][MAX_BUFFER_SIZE], int iResolvedAdminGroups)
{
	int	y = 0;
	int	z = 0;

	while (y < MAXPLAYERS+1)
	{
		resolvedAdminGroups[y] = "";
		groups[y] = UNDEFINED_GROUP_ID;

		z = 0;
		while (z < MAXPLAYERS+1)
		{
			names[y][z] = UNDEFINED_ADMIN_ID;
			z++;
		}
		y++;
	}
}

public void getAdminsAndGroups(GroupId[] groups, AdminId[][] names)
{
	char group[64];

	int	i = 0;
	int	y = 0;
	int	z = 0;
	int j = 0;

	i = 1;
	while (i <= MaxClients)
	{
		if (IsValidClient(i))
		{
			AdminId aid = GetUserAdmin(i);

			if (GetAdminFlag(aid, Admin_Generic))
			{
				j = 0;
				int iGroupCount = GetAdminGroupCount(aid);
				GroupId gid = INVALID_GROUP_ID;
				while (j < iGroupCount)
				{
					gid = GetAdminGroup(aid, j, group, sizeof(group));
					if (gid != INVALID_GROUP_ID && GetAdmGroupAddFlag(gid, Admin_Generic))
						break;
					j++;
				}

				if (j >= iGroupCount)
				{
					i++;
					continue;
				}

				y = 0;
				while (groups[y] != UNDEFINED_GROUP_ID)
				{
					if (groups[y] == gid)
					{
						z = 0;
						while (names[y][z] != UNDEFINED_ADMIN_ID)
							z++;
						if (gid == INVALID_GROUP_ID)
							names[y][0] = view_as<AdminId>(i);
						else
							names[y][z] = aid;
						break;
					}
					y++;
				}

				if (groups[y] == UNDEFINED_GROUP_ID)
				{
					groups[y] = gid;
					if (gid == INVALID_GROUP_ID)
						names[y][0] = view_as<AdminId>(i);
					else
						names[y][0] = aid;
				}
			}
		}
		i++;
	}
}

public void resolveAdminsAndGroups(GroupId[] groups, AdminId[][] names, char resolvedAdminGroups[MAXPLAYERS+1][MAX_BUFFER_SIZE], int &resolvedAdminGroupsLength)
{
	char bufferName[MAX_NAME_LENGTH];
	char bufferAdminName[MAX_NAME_LENGTH];
	char name[MAX_NAME_LENGTH];

	char group[64];
	char groupColor[16];
	char buffer[MAX_BUFFER_SIZE];

	resolvedAdminGroupsLength = 0;
	int	y = 0;

	while (groups[resolvedAdminGroupsLength] != UNDEFINED_GROUP_ID)
	{
		y = 0;

		GroupId gid = GetAdminGroup(names[resolvedAdminGroupsLength][y], 0, group, sizeof(group));
		if (gid == INVALID_GROUP_ID)
			group = "Admin";
		else if (StrEqual(group, "VIP"))
			gid = GetAdminGroup(names[resolvedAdminGroupsLength][y], 1, group, sizeof(group));

		if (StrEqual(group, "Server Root")) Format(groupColor, sizeof(groupColor), "{fullred}");
		else if (StrEqual(group, "Technical Administrator")) Format(groupColor, sizeof(groupColor), "{gold}");
		else if (StrEqual(group, "Server Manager")) Format(groupColor, sizeof(groupColor), "{cyan}");
		else if (StrEqual(group, "Lead Server Admin")) Format(groupColor, sizeof(groupColor), "{orange}");
		else if (StrEqual(group, "Server Admin")) Format(groupColor, sizeof(groupColor), "\x0745da4a");
		else if (StrEqual(group, "Donated Admin")) Format(groupColor, sizeof(groupColor), "\x0745da4a");
		else if (StrEqual(group, "Trial Admin")) Format(groupColor, sizeof(groupColor), "{lightgreen}");
		else Format(groupColor, sizeof(groupColor), "{blue}");

		Format(buffer, sizeof(buffer), "%s[%s] \x05", groupColor, group);

		while (names[resolvedAdminGroupsLength][y] != UNDEFINED_ADMIN_ID)
		{
			if (gid == INVALID_GROUP_ID && !GetClientName(view_as<int>(names[resolvedAdminGroupsLength][y]), bufferName, sizeof(bufferName)))
			{
				Format(bufferName, sizeof(bufferName), "Disconnected: %d", names[resolvedAdminGroupsLength][y]);
				Format(name, sizeof(name), "%s", bufferName);
			}
			else
			{
				names[resolvedAdminGroupsLength][y].GetUsername(bufferAdminName, sizeof(bufferAdminName));
				if (g_cAdminsRealNames.BoolValue && GetClientNameOfAdminId(names[resolvedAdminGroupsLength][y], bufferName, sizeof(bufferName)))
					Format(name, sizeof(name), "%s (%s)", bufferAdminName, bufferName);
				else
					Format(name, sizeof(name), "%s", bufferAdminName);
			}

			if (y == 0)
				StrCat(buffer, sizeof(buffer), name);
			else
			{
				StrCat(buffer, sizeof(buffer), "\x04, \x05");
				StrCat(buffer, sizeof(buffer), name);
			}
			y++;
		}
		resolvedAdminGroups[resolvedAdminGroupsLength] = buffer;
		resolvedAdminGroupsLength++;
	}
}

public int GetClientOfAdminId(AdminId aid)
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsValidClient(i))
		{
			AdminId foundAid = GetUserAdmin(i);
			if (aid == foundAid)
				return i;
		}
	}
	return 0;
}

public bool GetClientNameOfAdminId(AdminId aid, char[] name, int maxlen)
{
	int client = GetClientOfAdminId(aid);
	if (!client)
		return false;
	return GetClientName(client, name, maxlen);
}

bool IsValidClient(int client, bool nobots = true)
{
	if (client <= 0 || client > MaxClients || !IsClientConnected(client) || (nobots && IsFakeClient(client)))
	{
		return false;
	}
	return IsClientInGame(client);
}