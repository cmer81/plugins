/**
 * Maximum length of a weapon name string
 */
#define WEAPONS_MAX_LENGTH 32

/**
 * Number of weapon slots (For CS:S)
 */
#define WEAPONS_SLOTS_MAX 5

/**
 * Weapon slots.
 */
enum WeaponsSlot
{
    Slot_Invalid        = -1,   /** Invalid weapon (slot). */
    Slot_Primary        = 0,    /** Primary weapon slot. */
    Slot_Secondary      = 1,    /** Secondary weapon slot. */
    Slot_Melee          = 2,    /** Melee (knife) weapon slot. */
    Slot_Projectile     = 3,    /** Projectile (grenades, flashbangs, etc) weapon slot. */
    Slot_Explosive      = 4,    /** Explosive (c4) weapon slot. */
    Slot_MAX,
}

enum AmmoType
{
    Ammo_Clip1 = 0,
    Ammo_Reserve,
}

stock int WeaponsGetClientWeapons(int client, int weapons[Slot_MAX])
{
    // x = Weapon slot.
    for (int x = 0; x < WEAPONS_SLOTS_MAX; x++)
    {
        weapons[x] = GetPlayerWeaponSlot(client, x);
    }
}

stock int WeaponsGetClientWeaponIdByWeaponName(int client, const char[] sWeaponName)
{
    // Get all of client's current weapons.
    int weapons[Slot_MAX];
    WeaponsGetClientWeapons(client, weapons);

    char classname[64];

    // x = slot index
    for (int x = 0; x < WEAPONS_SLOTS_MAX; x++)
    {
        // If slot is empty, then stop.
        if (weapons[x] == -1)
            continue;
        
        // If the weapon's classname matches, then return true.
        GetEdictClassname(weapons[x], classname, sizeof(classname));
        ReplaceString(classname, sizeof(classname), "weapon_", "");
        if (StrEqual(sWeaponName, classname, false))
        {
            return weapons[x];
        }
    }    
    return false;
}

stock int GetWeaponOwner(int weapon)
{
    int m_hOwner = FindSendPropInfo("CBaseCombatWeapon", "m_hOwner");
    return GetEntDataEnt2(weapon, m_hOwner);
}

stock int GetClientActiveWeapon(int client)
{
    int m_hActiveWeapon = FindSendPropInfo("CBasePlayer", "m_hActiveWeapon");
    return GetEntDataEnt2(client, m_hActiveWeapon);
}

// int m_iClip2 = FindSendPropInfo("CBaseCombatWeapon", "m_iClip2");
// int m_hMyWeapons = FindSendPropInfo("CBasePlayer", "m_hMyWeapons");

stock int GetAmmo(int client, int weapon, AmmoType type)
{
    int m_iClip1 = FindSendPropInfo("CBaseCombatWeapon", "m_iClip1");
    int m_iAmmo = FindSendPropInfo("CBasePlayer", "m_iAmmo");
    int m_iPrimaryAmmoType = FindSendPropInfo("CBaseCombatWeapon", "m_iPrimaryAmmoType");
    // int m_iSecondaryAmmoType = FindSendPropInfo("CBaseCombatWeapon", "m_iSecondaryAmmoType");

    switch (type)
    {
        case Ammo_Clip1:
        {
            return GetEntData(weapon, m_iClip1);
        }
        case Ammo_Reserve:
        {
            int WeaponID = GetEntData(weapon, m_iPrimaryAmmoType) * 4;
            return GetEntData(client, m_iAmmo + WeaponID);
        }
    }
    return -1;
}

stock void SetAmmo(int client, int weapon, int ammo, AmmoType type)
{
    int m_iClip1 = FindSendPropInfo("CBaseCombatWeapon", "m_iClip1");
    int m_iAmmo = FindSendPropInfo("CBasePlayer", "m_iAmmo");
    int m_iPrimaryAmmoType = FindSendPropInfo("CBaseCombatWeapon", "m_iPrimaryAmmoType");
    // int m_iSecondaryAmmoType = FindSendPropInfo("CBaseCombatWeapon", "m_iSecondaryAmmoType");

    switch (type)
    {
        case Ammo_Clip1:
        {
            SetEntData(weapon, m_iClip1, ammo, _, true);
        }
        case Ammo_Reserve:
        {
            int WeaponID = GetEntData(weapon, m_iPrimaryAmmoType) * 4;
            SetEntData(client, m_iAmmo + WeaponID, ammo);
            // SetEntData(weapon, m_iSecondaryAmmoType, ammo);
        }
    }
}
