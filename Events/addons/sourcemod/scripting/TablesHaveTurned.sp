#pragma semicolon 1

#include <sourcemod>
#include <multicolors>
#include <Events>
#include <zombiereloaded>

#pragma newdecls required

bool g_bEventStarted = false;

public Plugin myinfo =
{
    name = "[Event] TablesHaveTurned",
    author = "maxime1907",
    description = "Humans have 0 cash, 0 armor and Zombies have an AK47, no knife",
    version = "1.0",
    url = ""
};

public void Events_OnEventsLoaded()
{
    //g_bEventStarted = Events_RegisterEvent();
}

public void OnPluginStart()
{
    g_bEventStarted = Events_RegisterEvent();
    if (!g_bEventStarted)
        return;

    HookEvent("player_spawn", OnPlayerSpawn, EventHookMode_Pre);
}

public void OnPluginEnd()
{
    if (!g_bEventStarted)
        return;

    UnhookEvent("player_spawn", OnPlayerSpawn, EventHookMode_Pre);
}

// ##     ##  #######   #######  ##    ##  ######  
// ##     ## ##     ## ##     ## ##   ##  ##    ## 
// ##     ## ##     ## ##     ## ##  ##   ##       
// ######### ##     ## ##     ## #####     ######  
// ##     ## ##     ## ##     ## ##  ##         ## 
// ##     ## ##     ## ##     ## ##   ##  ##    ## 
// ##     ##  #######   #######  ##    ##  ######  

public Action Timer_DelayArmor(Handle timer, any serialClient)
{
    int client = GetClientFromSerial(serialClient);
    if (!IsValidClient(client))
        return;

    int userid = GetClientUserId(client);
    ServerCommand("sm_armor #%d 0", userid);
}

stock void OnPlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
    int userid = GetEventInt(event, "userid");
    int client = GetClientOfUserId(userid);

    if (!IsValidClient(client))
        return;

    ServerCommand("sm_cash #%d 0", userid);

    CreateTimer(2.0, Timer_DelayArmor, GetClientSerial(client));
}

public void ZR_OnClientInfected(int client, int attacker, bool motherInfect, bool respawnOverride, bool respawn)
{
    if (!g_bEventStarted)
        return;

    if (!IsValidClient(client))
        return;

    int userid = GetClientUserId(client);
    ServerCommand("sm_strip #%d", userid);
    ServerCommand("sm_give #%d ak47", userid);
}

// ######## ##     ## ##    ##  ######  ######## ####  #######  ##    ##  ######  
// ##       ##     ## ###   ## ##    ##    ##     ##  ##     ## ###   ## ##    ## 
// ##       ##     ## ####  ## ##          ##     ##  ##     ## ####  ## ##       
// ######   ##     ## ## ## ## ##          ##     ##  ##     ## ## ## ##  ######  
// ##       ##     ## ##  #### ##          ##     ##  ##     ## ##  ####       ## 
// ##       ##     ## ##   ### ##    ##    ##     ##  ##     ## ##   ### ##    ## 
// ##        #######  ##    ##  ######     ##    ####  #######  ##    ##  ######

bool IsValidClient(int client, bool nobots = true)
{
    if (client <= 0 || client > MaxClients || !IsClientConnected(client) || (nobots && IsFakeClient(client)))
    {
        return false;
    }
    return IsClientInGame(client);
}